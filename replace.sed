/%%SCRIPT%%/ {
  r wrapper.bc.js
  d
}

/%%NORMALIZE%%/ {
  r ../../../css/normalize.css
  d
}

/%%SKELETON%%/ {
  r ../../../css/skeleton.css
  d
}
