open Utils

let raise_error buf msg =
  raise (Error (Printf.sprintf "At offset %d: %s.\n%!" (Lexing.lexeme_start buf) msg))

let read : string -> unit Music.t =
  fun content ->
    let buf = Lexing.from_string content in
    try
      Mparser.music Mlexer.read buf
    with
    | Mlexer.SyntaxError msg -> raise_error buf msg
    | Mparser.Error ->
      raise_error buf "syntax error"
