type t

val of_int : int -> t

val pp : Format.formatter -> t -> unit

val show : t -> string

val dot : t -> t

val tuplet : int -> int -> t -> t

val duration : int -> t -> Num.num

val duration_in_seconds : bpm:Num.num -> int -> t -> Num.num

val value : t -> int

val dots : t -> int

val tuplet_levels : t -> int

val tuplet_level : t -> int -> int * int
