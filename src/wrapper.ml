open Utils
open Cage
open Core_kernel
module Js = Js_of_ocaml.Js

let run_str cfg notes =
  let buffer = Buffer.create 32 in
  run cfg (Buffer.add_string buffer) notes;
  Buffer.contents buffer |> Js.string

let js_to_num x =
  let flt = Js.to_float x *. 1000.0 in
  let dec = int_of_float flt in
  Num.(num_of_int dec // thousand)

let with_handler f =
  try f () with e -> "ERROR: " ^ Exn.to_string e |> Js.string

let config interval_factor duration_factor =
  {
    interval_scale = (fun x -> Num.(x */ js_to_num interval_factor));
    duration_scale = (fun x -> Num.(x */ js_to_num duration_factor));
  }

let js_obj =
  object%js
    method random interval_factor duration_factor n =
      with_handler (fun () ->
          let n = Js.to_float n |> int_of_float in
          let notes = Random_gen.generate n in
          run_str (config interval_factor duration_factor) notes)

    method analysis interval_factor duration_factor input =
      with_handler (fun () ->
          let notes = Reader.read (Js.to_string input) in
          run_str (config interval_factor duration_factor) notes)
  end

let () = Js.export "cage" js_obj
