%token LEFT_PAREN
%token RIGHT_PAREN
%token <int> INTEGER
%token SLASH
%token TILDE
%token PIPE
%token <Music.pitch_class> PITCH_CLASS
%token R
%token TICK
%token COMMA
%token COLON
%token DOT
%token TEMPO
%token CLEF
%token EQUAL
%token EOF

%start <unit Music.t> music

%{ open Utils %}

%%

music:
  | b = bar_seq EOF { b }
  | b = bar_seq PIPE EOF { b }
  ;

bar_seq:
  | b = bar_with_ts { Vec.singleton b }
  | m = bar_seq PIPE b = note_seq { Music.add_bar_same_ts m b }
  | m = bar_seq PIPE b = bar_with_ts { Music.add_bar m b }
  ;

bar_with_ts:
  | pair = time_sig n = note_seq {
      match pair with
      | None, ts -> Music.bar_exn ts n
      | tc, ts -> Music.bar_exn ts (Utils.modify_first (fun x->
          match x.Music.tempo_change with
          | None -> {x with Music.tempo_change = tc}
          | _ -> x)
        n)
    }
  ;

note_seq:
  | n = note { Vec.singleton n }
  | LEFT_PAREN ratio = tuplet_ratio tuplets = note_seq RIGHT_PAREN
    { Music.tupletize (fst ratio) (snd ratio) tuplets }
  | ns = note_seq n = note {
      let last = Vec.peek_back_exn ns in
      match last.Music.tie with
      | Music.Tie_right | Music.Tie_both -> ns >% (Music.tie_left last n)
      | _ -> ns >% n
    }
  | ns = note_seq LEFT_PAREN ratio = tuplet_ratio tuplets = note_seq RIGHT_PAREN
    {
      let tupletized = Music.tupletize (fst ratio) (snd ratio) tuplets in
      let last = Vec.peek_back_exn ns in
      match last.Music.tie with
      | Music.Tie_right | Music.Tie_both ->
        let tied = modify_first (fun x -> (Music.tie_left last x)) tupletized in
        vconcat ns tied
      | _ -> vconcat ns tupletized
    }
  ;

note_attachment:
  | tc = tempo_change { (Some tc, None) }
  | c = clef { (None, Some c) }
  | tc = tempo_change c = clef { (Some tc, Some c) }
  | c = clef tc = tempo_change { (Some tc, Some c) }
  ;

note:
  | n = pitchednote { n }
  | na = note_attachment  n = pitchednote { let (tc, c) = na in { n with clef_change = c; tempo_change = tc } }
  | R fig = figure { Music.rest fig }
  | na = note_attachment R fig = figure { let (tc, c) = na in Music.rest ?clef_change:c ?tempo_change:tc fig }
  | n = pitchednote TILDE { { n with Music.tie = Music.Tie_right } }
  | na = note_attachment n = pitchednote TILDE { let (tc, c) = na in { n with
                                                tempo_change = tc;
                                                clef_change = c;
                                                Music.tie = Music.Tie_right } }
  ;

pitchednote:
  | pc = PITCH_CLASS fig = figure { Music.note pc 4 fig }
  | pc = PITCH_CLASS oct = octave fig = figure { Music.note pc (4 + oct) fig }
  ;

octave:
  | n = octave_ticks { n }
  | n = octave_commas { n }
  ;

octave_ticks:
  | TICK { 1 }
  | n = octave_ticks TICK { n + 1 }
  ;

octave_commas:
  | COMMA { -1 }
  | n = octave_commas COMMA { n - 1 }
  ;

tempo_change:
  | TEMPO figure = INTEGER EQUAL bpm = INTEGER { Music.tempo_change figure (float_of_int bpm) }
  ;

clef:
  | CLEF pc = PITCH_CLASS {
    match pc with
    | (Music.C, 0) -> Music.C_clef
    | (Music.F, 0) -> Music.F_clef
    | (Music.G, 0) -> Music.G_clef
    | _ -> failwith "Invalid clef"
  }
  ;

time_sig:
  | tc = tempo_change num = INTEGER SLASH den = INTEGER { Some tc, Music.time_sig num den }
  | num = INTEGER SLASH den = INTEGER { None, Music.time_sig num den }
  ;

tuplet_ratio:
  | num = INTEGER COLON den = INTEGER { (num, den) }

figure:
  | n = INTEGER { Figure.of_int n }
  | f = figure DOT { Figure.dot f }
  ;
