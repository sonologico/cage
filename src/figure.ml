open Utils
open Core_kernel

type t = { value : int; dots : int; tuplet : (int * int) list } [@@deriving show]

let of_int : int -> t =
 fun value ->
  ensure_power_of_2 value;
  { value; dots = 0; tuplet = [] }

let dot : t -> t = fun x -> { x with dots = x.dots + 1 }

let tuplet : int -> int -> t -> t =
 fun num in_place_of x ->
  ensure_positive num "tuplet num";
  ensure_positive in_place_of "tuplet in_place_of";
  { x with tuplet = (num, in_place_of) :: x.tuplet }

let value : t -> int = fun { value; _ } -> value

let dots : t -> int = fun { dots; _ } -> dots

let duration : int -> t -> Num.num =
 fun beat { value; dots; tuplet } ->
  let open Num in
  let base = num_of_int beat // num_of_int value in
  let dotted = base */ ((one -/ (half **/ (one +/ num_of_int dots))) // half) in
  let f acc (num, in_place_of) =
    num_of_int in_place_of */ acc // num_of_int num
  in
  List.fold ~f ~init:dotted tuplet

let sixty = Num.num_of_int 60

let duration_in_seconds ~bpm beat fig =
   Num.(duration beat fig // (bpm // sixty))

let tuplet_levels : t -> int = fun { tuplet; _ } -> List.length tuplet

let tuplet_level : t -> int -> int * int =
 fun { tuplet; _ } i -> List.nth_exn tuplet i
