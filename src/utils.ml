open Core_kernel
module Vec = Fdeque

exception Error of string

let zero : Num.num = Num.num_of_int 0

let one : Num.num = Num.num_of_int 1

let half : Num.num = Num.(one // Num.num_of_int 2)

let thousand : Num.num = Num.num_of_int 1000

let log2 x = Float.log10 x /. Float.log10 2.0

let is_power_of_2 x =
  x > 0
  &&
  let log = x |> float_of_int |> log2 in
  let rounded = Float.round_down log in
  Float.equal log rounded

let ensure_power_of_2 : int -> unit =
 fun x ->
  if is_power_of_2 x then ()
  else raise (invalid_arg ("Invalid figure: " ^ string_of_int x))

let ensure_positive : int -> string -> unit =
 fun x str ->
  if x > 0 then () else raise (invalid_arg (str ^ ":" ^ string_of_int x))

let ( >% ) v x = Vec.enqueue_back v x

let vmap2 : ('a -> 'b -> 'c) -> 'a Vec.t -> 'b Vec.t -> 'c Vec.t =
  let rec loop acc f a b =
    if Vec.is_empty a || Vec.is_empty b then acc
    else
      let x, a = Vec.dequeue_front_exn a in
      let y, b = Vec.dequeue_front_exn b in
      loop (Vec.enqueue_back acc (f x y)) f a b
  in
  fun f a b -> loop Vec.empty f a b

let rec vconcat : 'a Vec.t -> 'a Vec.t -> 'a Vec.t =
 fun a b ->
  match Vec.dequeue_front b with
  | None -> a
  | Some (x, b) -> vconcat (Vec.enqueue_back a x) b

let modify_first : ('a -> 'a) -> 'a Vec.t -> 'a Vec.t =
 fun f vec ->
  let x, xs = Vec.dequeue_front_exn vec in
  Vec.enqueue_front xs (f x)

let modify_last : ('a -> 'a) -> 'a Vec.t -> 'a Vec.t =
 fun f vec ->
  let x, xs = Vec.dequeue_back_exn vec in
  Vec.enqueue_back xs (f x)

type pos = First_elem | Middle_elem | Last_elem | Single_elem

let vfold : (pos -> 'a -> 'b -> 'a) -> 'a -> 'b Vec.t -> 'a =
  let rec loop f acc vec =
    let x, vec = Vec.dequeue_front_exn vec in
    if Vec.is_empty vec then f Last_elem acc x
    else loop f (f Middle_elem acc x) vec
  in
  fun f init vec ->
    match Vec.dequeue_front vec with
    | None -> init
    | Some (x, vec) when Vec.is_empty vec -> f Single_elem init x
    | Some (x, vec) -> loop f (f First_elem init x) vec

let vinit : int -> (int -> 'a) -> 'a Vec.t =
  let rec loop acc f = function
    | 0 -> acc
    | i -> loop (acc >% f i) f (i - 1)
  in
  fun n f -> loop Vec.empty f n

let ignore_fun : ('a -> 'b) -> unit = fun _ -> ()
