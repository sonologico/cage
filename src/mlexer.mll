{
open Lexing
open Mparser

exception SyntaxError of string

let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
               pos_lnum = pos.pos_lnum + 1
    }
}

let int = ['0' - '9'] ['0' - '9']*
let white = [' ' '\t']*
let newline = '\r' | '\n' | "\r\n"
let sharp = "is"+
let flat = "es"+
let alteration = sharp | flat
let pitch_class = ['a' - 'g'] alteration?
let tempo = "tempo"
let clef = "clef"

rule read = parse
| white { read lexbuf }
| newline { next_line lexbuf; read lexbuf }
| tempo { TEMPO }
| clef { CLEF }
| int { INTEGER (int_of_string (Lexing.lexeme lexbuf))}
| pitch_class { PITCH_CLASS (Music.pitch_class_of_string_exn (Lexing.lexeme lexbuf)) }
| '=' { EQUAL }
| '/' { SLASH }
| '|' { PIPE }
| '\'' { TICK }
| ',' { COMMA }
| '(' { LEFT_PAREN }
| ')' { RIGHT_PAREN }
| '~' { TILDE }
| '.' { DOT }
| ':' { COLON }
| 'r' { R }
| _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
| eof { EOF }
