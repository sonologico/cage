open Utils
open Core_kernel

type clef = C_clef | F_clef | G_clef

type tempo_change = { figure : int; bpm : float }

type time_sig = { num : int; beat : int }

type tie = Untied | Tie_left | Tie_right | Tie_both

type pitch_name = C | D | E | F | G | A | B [@@deriving show]

type pitch_class = pitch_name * int [@@deriving show]

type pitch = { pclass : pitch_class; octave : int } [@@deriving show]

type 'a note = {
  pitch : pitch option;
  figure : Figure.t;
  tie : tie;
  tuplet_boundary : int;
  tempo_change : tempo_change option;
  clef_change : clef option;
  annotation : 'a;
}

type 'a bar = time_sig option * 'a note Vec.t

type 'a t = 'a bar Vec.t

let pitch_class_of_int : int -> pitch_class =
  let arr =
    [|
      (C, 0);
      (D, -1);
      (D, 0);
      (E, -1);
      (E, 0);
      (F, 0);
      (G, -1);
      (G, 0);
      (A, -1);
      (A, 0);
      (B, -1);
      (B, 0);
    |]
  in
  let rec f n = if n < 0 then f (n + 12) else arr.(n mod 12) in
  f

let time_sig : int -> int -> time_sig =
 fun num beat ->
  ensure_power_of_2 beat;
  ensure_positive num "Invalid time sig numerator";
  { num; beat }

let tempo_change : int -> float -> tempo_change =
 fun beat bpm ->
  ensure_power_of_2 beat;
  if Float.(bpm > 0.) then { figure = beat; bpm } else invalid_arg "negative bpm"

let bar_exn : time_sig -> 'a note Vec.t -> 'a bar =
 fun ts vec ->
  let total = Num.num_of_int ts.num in
  let f acc x = Num.sub_num acc (Figure.duration ts.beat x.figure) in
  let diff = Vec.fold ~f ~init:total vec in
  if Num.eq_num diff zero then (Some ts, vec)
  else raise (Error "bar has wrong length")

let bar ts vec = try Some (bar_exn ts vec) with _ -> None

let int_of_pitch_class (name, alter) =
  ( alter
  +
  match name with
  | C -> 0
  | D -> 2
  | E -> 4
  | F -> 5
  | G -> 7
  | A -> 9
  | B -> 11 )
  mod 12

let enharmonic a b =
  a.octave = b.octave
  && int_of_pitch_class a.pclass = int_of_pitch_class b.pclass

let string_of_pitch_class (name, alter) =
  let base =
    match name with
    | C -> "c"
    | D -> "d"
    | E -> "e"
    | F -> "f"
    | G -> "g"
    | A -> "a"
    | B -> "b"
  in
  base
  ^
  if alter > 0 then
    String.init (alter * 2) ~f:(fun i -> if i mod 2 = 0 then 'i' else 's')
  else
    String.init (abs alter * 2) ~f:(fun i -> if i mod 2 = 0 then 'e' else 's')

let pitch_name_of_char_exn = function
  | 'c' -> C
  | 'd' -> D
  | 'e' -> E
  | 'f' -> F
  | 'g' -> G
  | 'a' -> A
  | 'b' -> B
  | c -> raise (Error ("invalid pitch_name char '" ^ String.of_char c ^ "'"))

let pitch_class_of_string_exn s =
  let rec count_alterations ch length = function
    | i when i = length -> (length - 1) / 2 * if Char.equal ch 'i' then 1 else -1
    | i ->
        if Char.(s.[i] = ch && s.[i + 1] = 's') then count_alterations ch length (i + 2)
        else raise (Error ("invalid pitch_name_string '" ^ s ^ "'"))
  in
  let pitch_name = pitch_name_of_char_exn s.[0] in
  let alteration =
    match String.length s with
    | 1 -> 0
    | _ when Char.(s.[1] = 'i' || s.[1] = 'e') ->
        count_alterations s.[1] (String.length s) 1
    | _ -> raise (Error ("invalid pitch_name_string '" ^ s ^ "'"))
  in
  (pitch_name, alteration)

let bar_time_sig = fst

let bar_content = snd

let time_sig_num { num; _ } = num

let time_sig_beat { beat; _ } = beat

let time_sig_equal a b = a.num = b.num && a.beat = b.beat

let note_duration ts { figure; _ } = Figure.duration ts.beat figure

let fold (music : 'a t) f init =
  let bar_f acc (ts, notes) =
    Vec.fold ~f:(fun acc x -> f ts acc x) ~init:acc notes
  in
  Vec.fold ~f:bar_f ~init music

let annotate_note note ann = { note with annotation = ann }

let annotate f init music =
  let bar_f (acc, result_music) (ts, notes) =
    let acc, content =
      Vec.fold
        ~f:(fun (acc, content) x ->
          let acc, annotation = f acc ts x in
          (acc, content >% annotate_note x annotation))
        ~init:(acc, Vec.empty) notes
    in
    (acc, result_music >% (ts, content))
  in
  Vec.fold ~f:bar_f ~init:(init, Vec.empty) music

let dot_note note = { note with figure = Figure.dot note.figure }

let tupletize num dem vec =
  let f note = { note with figure = Figure.tuplet num dem note.figure } in
  let incr_first x = { x with tuplet_boundary = x.tuplet_boundary + 1 } in
  let decr_last x = { x with tuplet_boundary = x.tuplet_boundary - 1 } in
  modify_first incr_first (modify_last decr_last (Vec.map ~f vec))

let tie_left prev x =
  let add_left = function Tie_right -> Tie_both | _ -> Tie_left in
  match (prev.pitch, x.pitch) with
  | Some p1, Some p2 when not (enharmonic p1 p2) ->
      raise
        (Error
           ( "tie between different pitches: " ^ show_pitch p1 ^ "   "
           ^ show_pitch p2 ))
  | None, _ | _, None -> raise (Error "Tie involving rest")
  | _, _ -> { x with tie = add_left x.tie }

let note ?clef_change ?tempo_change ?(tie = Untied) pclass octave figure =
  {
    pitch = Some { pclass; octave };
    figure;
    tie;
    tuplet_boundary = 0;
    tempo_change;
    clef_change;
    annotation = ();
  }

let rest ?clef_change ?tempo_change figure =
  {
    pitch = None;
    figure;
    tie = Untied;
    tuplet_boundary = 0;
    tempo_change;
    clef_change;
    annotation = ();
  }

let unmetered_bar : 'a note Vec.t -> 'a bar = fun notes -> (None, notes)

let add_bar : 'a t -> 'a bar -> 'a t =
 fun music ((ts, content) as bar) ->
  let last = Vec.peek_back_exn (bar_content (Vec.peek_back_exn music)) in
  music
  >%
  match last.tie with
  | Tie_right | Tie_both -> (ts, modify_first (tie_left last) content)
  | _ -> bar

let add_bar_same_ts : 'a t -> 'a note Vec.t -> 'a t =
 fun music notes ->
  add_bar music
    ( match bar_time_sig (Vec.peek_back_exn music) with
    | None -> unmetered_bar notes
    | Some ts -> bar_exn ts notes )
