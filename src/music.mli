open Utils

type clef = C_clef | F_clef | G_clef

type tempo_change = private { figure : int; bpm : float }

type pitch_name = C | D | E | F | G | A | B

type pitch_class = pitch_name * int

type tie = Untied | Tie_left | Tie_right | Tie_both

type pitch = { pclass : pitch_class; octave : int }

type 'a note = {
  pitch : pitch option;
  figure : Figure.t;
  tie : tie;
  tuplet_boundary : int;
  tempo_change : tempo_change option;
  clef_change : clef option;
  annotation : 'a;
}

type time_sig

type 'a bar

type 'a t = 'a bar Vec.t

val pitch_class_of_int : int -> pitch_class

val int_of_pitch_class : pitch_class -> int

val string_of_pitch_class : pitch_class -> string

val pitch_class_of_string_exn : string -> pitch_class

val tempo_change : int -> float -> tempo_change

val time_sig : int -> int -> time_sig

val bar_exn : time_sig -> 'a note Vec.t -> 'a bar

val bar : time_sig -> 'a note Vec.t -> 'a bar option

val bar_time_sig : 'a bar -> time_sig option

val bar_content : 'a bar -> 'a note Vec.t

val time_sig_num : time_sig -> int

val time_sig_beat : time_sig -> int

val time_sig_equal : time_sig -> time_sig -> bool

val note_duration : time_sig -> 'a note -> Num.num

val fold : 'a t -> (time_sig option -> 'b -> 'a note -> 'b) -> 'b -> 'b

val annotate :
  ('b -> time_sig option -> 'a note -> 'b * 'c) -> 'b -> 'a t -> 'b * 'c t

val dot_note : 'a note -> 'a note

val tupletize : int -> int -> 'a note Vec.t -> 'a note Vec.t

val tie_left : 'a note -> 'a note -> 'a note

val note :
  ?clef_change:clef ->
  ?tempo_change:tempo_change ->
  ?tie:tie ->
  pitch_class ->
  int ->
  Figure.t ->
  unit note

val rest : ?clef_change:clef -> ?tempo_change:tempo_change -> Figure.t -> unit note

val add_bar : 'a t -> 'a bar -> 'a t

val add_bar_same_ts : 'a t -> 'a note Vec.t -> 'a t

val unmetered_bar : 'a note Vec.t -> 'a bar
