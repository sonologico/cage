open Utils
open Core_kernel
open Num

let level_labels = "cqgs"

let first_annotation = "c1q1g1s1"

type config = { interval_scale : num -> num; duration_scale : num -> num }

type level = {
  pitches : num array;
  durations : num array;
  intervals : num array;
  distances : num array;
  disjunctions : num array;
}

let intervals arr =
  Array.init
    (Array.length arr - 1)
    ~f:(fun i -> abs_num (arr.(i + 1) -/ arr.(i)))

let zip_add : num array -> num array -> num array =
 fun xs ys ->
  if Array.length xs < Array.length ys then
    Array.mapi xs ~f:(fun i x -> ys.(i) +/ x)
  else Array.mapi ys ~f:(fun i y -> xs.(i) +/ y)

let first_level cfg pitches durations =
  let intervals = intervals pitches in
  Array.iteri intervals ~f:(fun i x -> intervals.(i) <- cfg.interval_scale x);
  let durations = Array.map ~f:cfg.duration_scale durations in
  let distances = zip_add durations intervals in
  { pitches; durations; intervals; distances; disjunctions = distances }

let make_level previous pitches durations =
  let intervals = intervals pitches in
  let distances = zip_add durations intervals in
  let disjunctions = zip_add distances previous in
  { pitches; durations; intervals; distances; disjunctions }

let sum arr first last =
  let rec loop acc arr i last =
    if i = last then acc else loop (acc +/ arr.(i)) arr (i + 1) last
  in
  loop zero arr first last

let mean arr first last = sum arr first last // num_of_int (last - first)

let find_peaks arr =
  let indices = ref Vec.empty in
  let values = ref Vec.empty in
  for i = 1 to Array.length arr - 2 do
    if gt_num arr.(i) arr.(i - 1) && gt_num arr.(i) arr.(i + 1) then (
      indices := !indices >% i;
      values := !values >% arr.(i) )
  done;
  (Vec.to_array !indices, Vec.to_array !values)

let new_level previous =
  let indices, values = find_peaks previous.disjunctions in
  let start, pitches, durations =
    Array.fold
      ~f:(fun (start, pitches, durations) index ->
        let last = index + 1 in
        ( last,
          pitches >% mean previous.pitches start last,
          durations >% sum previous.durations start last ))
      ~init:(0, Vec.empty, Vec.empty) indices
  in
  let pitches_length = Array.length previous.pitches in
  let durations_length = Array.length previous.durations in
  let pitches = pitches >% mean previous.pitches start pitches_length in
  let durations = durations >% sum previous.durations start durations_length in
  make_level values (Vec.to_array pitches) (Vec.to_array durations)

type annotation_state = Middle of num | Begin of num

let default_ts = Music.time_sig 1 4

let default_tempo = Music.tempo_change 4 60.

let annotate cfg levels music =
  let incr_cur cur tempo note =
    cur
    +/ cfg.duration_scale
         (Figure.duration_in_seconds
            ~bpm:(Num.num_of_int (int_of_float tempo.Music.bpm))
            tempo.Music.figure note.Music.figure)
  in
  let i_cur_levels = Array.map ~f:(fun l -> (1, l.durations.(0))) levels in
  let middle cur tempo note =
    let i = ref 0 in
    let result =
      Array.filter_map
        ~f:(fun l ->
          let i_level, cur_level = i_cur_levels.(!i) in
          let result =
            if eq_num cur cur_level then (
              i_cur_levels.(!i) <-
                (i_level + 1, cur_level +/ l.durations.(i_level));
              Some
                (String.of_char level_labels.[!i] ^ string_of_int (i_level + 1))
              )
            else None
          in
          incr i;
          result)
        levels
      |> String.concat_array ~sep:""
    in
    ((tempo, Middle (incr_cur cur tempo note)), result)
  in
  let first cur tempo note =
    Array.iteri
      ~f:(fun i (p, c) -> i_cur_levels.(i) <- (p, c +/ cur))
      i_cur_levels;
    ((tempo, Middle (incr_cur cur tempo note)), first_annotation)
  in
  let annotate_f (tempo, state) _ts note =
    let tempo = Option.value ~default:tempo note.Music.tempo_change in
    match (state, note.Music.pitch) with
    | Begin cur, None -> ((tempo, Begin (incr_cur cur tempo note)), "")
    | Begin cur, Some _ -> first cur tempo note
    | Middle cur, _ -> middle cur tempo note
  in
  Music.annotate annotate_f (default_tempo, Begin zero) music |> snd

let int_of_pitch pitch =
  Music.int_of_pitch_class pitch.Music.pclass + (12 * pitch.Music.octave)

let split bars =
  let f _ts ((tempo, pitches, durations, tie_acc) as acc) x =
    let tempo = Option.value ~default:tempo x.Music.tempo_change in
    let duration =
      Figure.duration_in_seconds
        ~bpm:(Num.num_of_int (int_of_float tempo.bpm))
        tempo.figure x.figure
    in
    match x.Music.pitch with
    | None when Vec.is_empty pitches -> acc
    | None ->
        let durations = modify_last (fun d -> d +/ duration) durations in
        (tempo, pitches, durations, tie_acc)
    | Some pitch -> (
        match (x.Music.tie, tie_acc) with
        | Music.Untied, None ->
            ( tempo,
              pitches >% num_of_int (int_of_pitch pitch),
              durations >% duration,
              None )
        | Music.Tie_left, Some acc ->
            (tempo, pitches, durations >% acc +/ duration, None)
        | Music.Tie_right, None ->
            ( tempo,
              pitches >% num_of_int (int_of_pitch pitch),
              durations,
              Some duration )
        | Music.Tie_both, Some acc ->
            (tempo, pitches, durations, Some (acc +/ duration))
        | _, _ -> assert false )
  in
  let _, pitches, durations, dur_acc =
    Music.fold bars f (default_tempo, Vec.empty, Vec.empty, None)
  in
  match dur_acc with
  | None -> (Vec.to_array pitches, Vec.to_array durations)
  | Some dur -> (Vec.to_array pitches, Vec.to_array (durations >% dur))

let run cfg outch notes =
  let pitches, durations = split notes in
  let previous = ref (first_level cfg pitches durations) in
  let levels =
    Array.init (String.length level_labels) ~f:(fun _ ->
        let curr = new_level !previous in
        previous := curr;
        curr)
  in
  let annotated = annotate cfg levels notes in
  Lilypond.render outch annotated
