open Utils
open Core_kernel

let octave_marks = function
  | n when n > 3 -> String.make (n - 3) '\''
  | n -> String.make (3 - n) ','

type render_state = First | Unmetered | Metered of Music.time_sig

let render outch music =
  let render_state = ref First in
  let write_time_sig outch ts =
    render_state := Metered ts;
    outch "\\time ";
    outch (string_of_int (Music.time_sig_num ts));
    outch "/";
    outch (string_of_int (Music.time_sig_beat ts));
    outch "\n  "
  in
  let update_time_sig outch ts =
    match (!render_state, ts) with
    | Metered _, None | First, None ->
        render_state := Unmetered;
        outch "\\cadenzaOn\n  "
    | Unmetered, None -> ()
    | Metered prev_ts, Some curr_ts when Music.time_sig_equal prev_ts curr_ts ->
        ()
    | Unmetered, Some curr_ts ->
        outch "\\cadenzaOff\n  ";
        write_time_sig outch curr_ts
    | Metered _, Some curr_ts | First, Some curr_ts ->
        write_time_sig outch curr_ts
  in
  let note_f pos outch x =
    ( match x.Music.tempo_change with
    | None -> ()
    | Some { figure; bpm } ->
        outch " \\tempo ";
        outch (string_of_int figure);
        outch " = ";
        outch (string_of_int Float.(to_int (round bpm)));
        outch " " );
    ( match x.Music.clef_change with
    | None -> ()
    | Some C_clef -> outch " \\clef alto  "
    | Some F_clef -> outch " \\clef bass "
    | Some G_clef -> outch " \\clef treble ");
    let tuplet_boundary = x.Music.tuplet_boundary in
    if tuplet_boundary > 0 then
      for i = 0 to tuplet_boundary - 1 do
        let den, num = Figure.tuplet_level x.Music.figure i in
        outch "\\times ";
        outch (string_of_int num);
        outch "/";
        outch (string_of_int den);
        outch " {"
      done;
    if not (String.equal x.Music.annotation "") then
      outch "\\once \\override NoteHead #'color = #red ";
    ( match x.Music.pitch with
    | None -> outch "r"
    | Some pitch ->
        outch (Music.string_of_pitch_class pitch.Music.pclass);
        outch (octave_marks pitch.Music.octave) );
    outch (string_of_int (Figure.value x.Music.figure));
    for _ = 1 to Figure.dots x.Music.figure do
      outch "."
    done;
    if not (String.equal x.Music.annotation "") then (
      outch "^\\markup{";
      outch x.Music.annotation;
      outch "}" );
    ( match x.Music.tie with
    | Music.Tie_right | Music.Tie_both -> outch "~"
    | _ -> () );
    if tuplet_boundary < 0 then
      for _ = -1 downto tuplet_boundary do
        outch "}"
      done;
    ( match pos with
    | First_elem | Last_elem | Single_elem -> ()
    | Middle_elem -> (
        match !render_state with
        | Unmetered -> outch " \\bar \"\" "
        | Metered _ -> outch " "
        | First -> assert false ) );
    outch
  in
  let bar_f pos outch bar =
    update_time_sig outch (Music.bar_time_sig bar);
    ignore_fun (vfold note_f outch (Music.bar_content bar));
    ( match pos with
    | First_elem | Last_elem | Single_elem -> ()
    | Middle_elem -> outch " |\n  " );
    outch
  in
  outch "\\new Voice {\n  \\numericTimeSignature\n  ";
  ignore_fun (vfold bar_f outch music);
  outch "  \\bar \"|.\"\n}"
