open Utils
open Core_kernel
module Rnd = Random.State

let durations =
  let f xs =
    match xs with
    | [ x ] when Figure.tuplet_levels x > 0 ->
        (fst (Figure.tuplet_level x 0), xs)
    | _ -> (1, xs)
  in
  Array.map ~f
    [|
      [ Figure.tuplet 6 4 (Figure.of_int 16) ];
      [ Figure.tuplet 5 4 (Figure.of_int 16) ];
      [ Figure.of_int 16 ];
      [ Figure.tuplet 3 2 (Figure.of_int 8) ];
      [ Figure.tuplet 5 4 (Figure.of_int 8) ];
      [ Figure.of_int 8 ];
      [ Figure.tuplet 3 2 (Figure.of_int 4) ];
      [ Figure.dot (Figure.of_int 8) ];
      [ Figure.tuplet 5 4 (Figure.of_int 4) ];
      [ Figure.of_int 4 ];
      [ Figure.of_int 4; Figure.of_int 16 ];
      [ Figure.tuplet 3 2 (Figure.of_int 2) ];
      [ Figure.dot (Figure.of_int 4) ];
      [ Figure.of_int 4; Figure.dot (Figure.of_int 8) ];
      [ Figure.of_int 2 ];
      [ Figure.of_int 2; Figure.of_int 16 ];
      [ Figure.of_int 2; Figure.of_int 8 ];
      [ Figure.of_int 2; Figure.dot (Figure.of_int 8) ];
      [ Figure.dot (Figure.of_int 2) ];
      [ Figure.dot (Figure.of_int 2); Figure.of_int 16 ];
      [ Figure.dot (Figure.of_int 2); Figure.of_int 8 ];
      [ Figure.dot (Figure.of_int 2); Figure.dot (Figure.of_int 8) ];
      [ Figure.of_int 1 ];
    |]

let no_tuplets =
  Array.filter ~f:(function 1, _ -> true | _ -> false) durations

let dur_gen rnd = durations.(Rnd.int rnd (Array.length durations))

let dur_no_tuplets_gen rnd = no_tuplets.(Rnd.int rnd (Array.length no_tuplets))

let pc_gen rnd = Music.pitch_class_of_int (Rnd.int rnd 12)

let note ?(tie = Music.Untied) pc fig = Music.note ~tie pc 4 fig

let ref_ts = Music.time_sig 4 4

let duration : int * Figure.t list -> Num.num =
 fun (n, figs) ->
  let figs_dur =
    List.fold_left
      ~f:(fun acc x ->
        Num.(acc +/ Figure.duration (Music.time_sig_beat ref_ts) x))
      ~init:zero figs
  in
  Num.(num_of_int n */ figs_dur)

let rec gen rnd notes dur = function
  | n when n <= 0 -> notes
  | n ->
      let ((k, figs) as kfigs) =
        if Num.eq_num (Num.floor_num dur) dur then dur_gen rnd
        else dur_no_tuplets_gen rnd
      in
      let dur = Num.(dur +/ duration kfigs) in
      let notes =
        match (k, figs) with
        | 1, [ fig ] -> notes >% note (pc_gen rnd) fig
        | 1, tied_figs ->
            let pc = pc_gen rnd in
            let tied_notes =
              Vec.of_list (List.map ~f:(note ~tie:Music.Tie_both pc) tied_figs)
            in
            let tied_notes =
              modify_first
                (fun x -> { x with Music.tie = Music.Tie_right })
                tied_notes
            in
            let tied_notes =
              modify_last
                (fun x -> { x with Music.tie = Music.Tie_left })
                tied_notes
            in
            vconcat notes tied_notes
        | _, [ fig ] ->
            let tuplets = vinit k (fun _ -> note (pc_gen rnd) fig) in
            let tuplets =
              modify_first
                (fun x -> { x with Music.tuplet_boundary = 1 })
                tuplets
            in
            let tuplets =
              modify_last
                (fun x -> { x with Music.tuplet_boundary = -1 })
                tuplets
            in
            vconcat notes tuplets
        | _, _ -> assert false
      in
      gen rnd notes dur (n - k)

let generate n =
  Vec.singleton
    (Music.unmetered_bar (gen (Rnd.make_self_init ()) Vec.empty zero n))
